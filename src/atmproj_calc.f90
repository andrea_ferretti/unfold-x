!
! Copyright (C) 2013 Pietro Bonfa' 
! This file is distributed under the terms of the
! GNU General Public License. 
! See http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------------
SUBROUTINE atmproj_calc( ispin, ipkp, natomproj, atmproj_map, atmproj_weight)
    !----------------------------------------------------------------------------
    !
    ! Calculates weight due to atomic projections on all the atomic 
    ! orbitals included in atmproj_map.
    !
    ! W_Km = sum_alpha | < Km | K alpha >| ^2
    !
    ! - atomic_proj.xml file is first read
    ! - for each band, projections squared moduli are summed up
    !
    ! ispin: index of the spin channel
    ! ipkp : index of K
    ! natomproj   : number of atomic orbitals to include in the sum
    ! atmproj_map : actual indexes of the requested atomic orbitals
    ! atmproj_weight : output weight
    !
    USE kinds,       ONLY: DP
    USE wvfct,       ONLY: nbnd
    USE read_proj,   ONLY: read_xml_proj
    USE io_files,    ONLY: restart_dir
    USE unfold_data, ONLY: projs, nkstot
    ! 
    IMPLICIT NONE
    !
    INTEGER,  INTENT(in) :: ispin, ipkp 
    INTEGER,  INTENT(in) :: natomproj
    INTEGER,  INTENT(in) :: atmproj_map(natomproj)
    REAL(DP), INTENT(out) :: atmproj_weight(nbnd)
    
    INTEGER  :: ibnd, ia
    INTEGER  :: ik_loc, ia_loc
    COMPLEX(DP) :: caux

    !
    ! projdos have already been read in
    ! atmproj_read
    !

    !
    ! main loop
    !
    ik_loc=ipkp
    if (ispin==2) ik_loc=ipkp+nkstot/2
    !
    !$omp parallel do default(shared), private(ibnd,ia,ia_loc,caux)
    DO ibnd = 1, nbnd
       atmproj_weight(ibnd)=0.0
       !
       DO ia = 1, natomproj
          !
          ia_loc=atmproj_map(ia)
          !
          caux=projs(ia_loc,ibnd,ik_loc)
          atmproj_weight(ibnd)=atmproj_weight(ibnd)+real(caux*conjg(caux),DP)
          !
       ENDDO
    ENDDO
    !$omp end parallel do
    ! 
END SUBROUTINE atmproj_calc
