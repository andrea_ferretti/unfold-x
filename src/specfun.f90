!
! Copyright (C) 2013 Pietro Bonfa'
! This file is distributed under the terms of the
! GNU General Public License.
! See http://www.gnu.org/copyleft/gpl.txt .
!
FUNCTION specfun(ens, pkml, E, w, use_atmproj, atmproj_weight) RESULT(r)
    !
    ! This subroutine calculates the specral function
    ! from P_Km coefficients
    !

    USE kinds,     ONLY: DP
    USE wvfct,     ONLY: nbnd
    USE constants, ONLY : pi

    IMPLICIT NONE
    REAL(DP), INTENT(in)  :: ens(nbnd)
    REAL(DP), INTENT(in)  :: pkml(nbnd)
    REAL(DP), INTENT(in)  :: E
    REAL(DP), INTENT(in)  :: w
    LOGICAL,  INTENT(in)  :: use_atmproj
    REAL(DP), INTENT(in)  :: atmproj_weight(nbnd)
    REAL(DP) :: r
    !
    INTEGER :: ibnd
    REAL(DP):: weight

    r = 0
    DO ibnd = 1, nbnd
        !
        weight=1.0
        IF (use_atmproj) weight=atmproj_weight(ibnd)
        !
        IF (ens(ibnd) > (E - 6*w) .and. ens(ibnd) < (E + 6*w) ) THEN
            r = r + weight*pkml(ibnd)*(1.d0/(w*sqrt(pi)))*exp(-(((E-ens(ibnd))**2)/(w**2))) ! pkml(ibnd)
        ENDIF
    ENDDO

END FUNCTION specfun

